﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace AppliedMathsAssessment
{
    public class PhysicsObject
    {
        // ------------------
        // Data
        // ------------------

        // Transform
        protected Vector3 rotation;
        protected Vector3 position;
        protected Vector3 scale = Vector3.One;

        // Physics
        protected BoundingBox hitBox;
        protected Vector3 velocity;
        protected Vector3 acceleration;
        protected float drag = 0.05f;
        protected Vector3 collisionScale = Vector3.One;
        protected Vector3 collisionOffSet = Vector3.Zero;
       // protected Vector3 ballVelocity = Vector3.Zero;
        protected bool isStatic = false;    // Not moved by physics
        protected bool useGravity = false;  // Falls with gravity each frame
        protected bool isTrigger = true;    // Does not trigger physics affects (but can still be sensed with collisions)
        protected float gravityScale = 1f;

        // Previous state
        protected Vector3 positionPrev;
        protected Vector3 velocityPrev;
        protected Vector3 accelerationPrev;

        // Numerical Integration
        public enum IntegrationMethod {
            EXPLICIT_EULER,
            METHOD_TWO,
            METHOD_THREE
        };
        // UPDATE THIS TO TEST TASK 6
        private IntegrationMethod currentIntegrationMethod = IntegrationMethod.EXPLICIT_EULER;

        // ------------------
        // Behaviour
        // ------------------
        public BoundingBox GetHitBox()
        {
            return hitBox;
        }
        // ------------------
        public virtual void UpdateHitBox()
        {
            // Just make a cube hitbox based on the scale
            hitBox = new BoundingBox(-collisionScale * 0.5f, collisionScale * 0.5f);

            // Move to correct position in game world
            hitBox.Min += position;
            hitBox.Max += position;
        }
        // ------------------
        public Vector3 GetPosition()
        {
            return position;
        }
        // ------------------
        public void SetPosition(Vector3 newPosition)
        {
            position = newPosition;
        }
        // ------------------
        public void SetScale(Vector3 newScale)
        {
            scale = newScale;
        }
        // ------------------
        public Vector3 GetGravityVector()
        {
            return new Vector3(0, -9.8f * gravityScale, 0);
        }
        // ------------------
        public virtual void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update acceleration due to gravity
            if (useGravity)
                acceleration.Y = -9.8f * gravityScale;

            // Store current state before making any modifications
            Vector3 positionCur = position;
            Vector3 velocityCur = velocity;
            Vector3 accelerationCur = acceleration;

            // Update velocity due to drag
            velocity *= (1.0f - drag);

            // Update velocity and position based on acceleration
            // Uses numerical integration (multiple possible methods)
            switch (currentIntegrationMethod)
            {
                case IntegrationMethod.EXPLICIT_EULER:
                    // This method is being deprecated due to stability issues.
                    position += velocity * dt;
                    velocity += acceleration * dt;
                    break;
                    
                ///////////////////////////////////////////////////////////////////
                //
                // CODE FOR TASK 6 SHOULD BE ENTERED HERE
                //
                ///////////////////////////////////////////////////////////////////
                case IntegrationMethod.METHOD_TWO:

                    // Insert method two code here
                    // Change value of currentIntegrationMethod to test this method


                    break;

                case IntegrationMethod.METHOD_THREE:

                    // Insert method three code here
                    // Change value of currentIntegrationMethod to test this method


                    break;

                ///////////////////////////////////////////////////////////////////  
                // END TASK 6 CODE
                ///////////////////////////////////////////////////////////////////  
            }

            // Store current state as previous state
            positionPrev = positionCur;
            velocityPrev = velocityCur;
            accelerationPrev = accelerationCur;

            // Update hitbox
            UpdateHitBox();
        }
        // ------------------
        public virtual void HandleCollision(PhysicsObject other)
        {
            // Don't react with physics if this object is static
            if (isStatic || isTrigger || other.isTrigger)
                return;

            ///////////////////////////////////////////////////////////////////
            //
            // CODE FOR TASK 3 SHOULD BE ENTERED HERE
            //
            ///////////////////////////////////////////////////////////////////

            //if this wall intersects with our ball...
            // if (walls[i].GetBoundingBox().Intersects(ball.GetDynamicAABB()))
            //{
            //we collide 
            //move back to the stored position 
            position = positionPrev;


                //get our boujding boxes for easy access 
                BoundingBox thisHitBox = hitBox;
                BoundingBox otherHitBox = other.hitBox;

                //calculate the collision depth 

                //get the distance between the centers of the player and other 
                Vector3 centerOther = otherHitBox.Min + (otherHitBox.Max - otherHitBox.Min) / 2.0f;
                Vector3 centerThis = thisHitBox.Min + (thisHitBox.Max - thisHitBox.Min) / 2.0f;
                Vector3 distance = centerOther - centerThis;

                //minimum distance these need to be to not ins=tersect 
                //aka if they are at least this far away in either direction, they are not touchign 
                Vector3 minDistance = (otherHitBox.Max - otherHitBox.Min) / 2.0f +
                                    (thisHitBox.Max - thisHitBox.Min) / 2.0f;

                if (distance.X < 0)
                    minDistance.X = -minDistance.X;
                if (distance.Y < 0)
                    minDistance.Y = -minDistance.Y;
                if (distance.Z < 0)
                    minDistance.Z = -minDistance.Z;


                //collision depth = how much over the minimum distance are we in eaxch direction 
                Vector3 depth = minDistance - distance;

                //with collision depth, we can now tell whcih axis(x,y, or z) are we collidign on
                //now we just need to get the plane representing the play of collision in that axis 

                //get the corners of the hitbox in an array 
                Vector3[] corners = otherHitBox.GetCorners();

                //vector onn tjhe plabe 
                Vector3 planeVector1;
                Vector3 planeVector2;

                //smallest depth  = direction of collision 
                if (Math.Abs(depth.Z) > Math.Abs(depth.X))
                {
                    // xis smaller, colliding in the x direction 

                    //use the west plane for collision 

                    //east and west are the same 
                    //choose EASt plane (positive x side ) for collision
                    //(both result insame calculation so no need to check 
                    //which side of the x axis we're colliding from 

                    //two vectors fromthe eas plane 
                    planeVector1 = corners[0] - corners[5];
                    planeVector2 = corners[6] - corners[5];
                }
                else
                {

                    // zis zmaller, so we're colliding on the z axis 

                    //choose the north plane 

                    //two vecotrs fromthe north plane 
                    planeVector1 = corners[1] - corners[0];
                    planeVector2 = corners[3] - corners[0];


                }

                //GetHashCode as VertexPositionNormalTexture vector use the cross product 
                Vector3 normal = Vector3.Cross(planeVector1, planeVector2);

                //normal vector are supposed to be unit vectors
                //length 1
                //so we need to use the normalise() funstion to chnage them to length 1 
                normal.Normalize();

                //reflect the players velocity off the surface using the normal 
                //this uses the dot product internally 
                velocity = Vector3.Reflect(velocity, normal);




                // OLD VERSION, DEPRECATED
                // Re-write this to account for more realistic collisions using 
                // the incoming angle
               // velocity *= -1;
               // position = positionPrev;

                ///////////////////////////////////////////////////////////////////  
                // END TASK 3 CODE
                ///////////////////////////////////////////////////////////////////  

        }
        // ------------------
        public virtual void Draw(Camera cam, DirectionalLightSource light)
        {

        }
        // ------------------
    }
}
